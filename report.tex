% !TeX encoding = ISO-8859-1
% !TeX spellcheck = es_ES

\documentclass[a4paper,sans, 12pt]{article}

\input{preamble}

\newcommand{\fecha}{2018-01-15}
\newcommand{\referencia}{}
\newcommand{\titulo}{\textbf{Trabajo de Aeroac�stica}\\\Large{Versi�n 14}}

\title{\titulo}
\author{Daniel Lubi�n Arenillas}
\date{\fecha}
\DeclareMathOperator{\ndims}{dim}
\addto\captionsspanish{\renewcommand{\listtablename}{�ndice de tablas}}
\addto\captionsspanish{\renewcommand{\tablename}{Tabla}}
\addto\captionsspanish{\renewcommand{\tan}{\mathop{\rm tg}\nolimits}}
\addto\captionsspanish{\renewcommand{\sin}{\mathop{\rm sen}\nolimits}} % Sustituye a \sin
\addto\captionsspanish{\renewcommand{\arctan}{\mathop{\rm arctg}\nolimits}}


\begin{document}
% \input{titlepage}

% \clearpage

\maketitle

\clearpage

\tableofcontents
\listoffigures
\listoftables

%% add this if you want the fancy style also on the first page of a chapter:
% \thispagestyle{fancy}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \clearpage

% \vspace{1cm}
% \hrule
% \vspace{0.5cm}
\clearpage

\section{Introducci�n}

El objetivo de este trabajo es aplicar el \emph{m�todo de los elementos finitos}, particularizado para el problema de Ac�stica.
Para ello, se resuelver un problema unidimensional del que se puede obtener su soluci�n anal�tica y se comparan ambas soluciones seg�n dos par�metros: la frecuencia de la onda de perturbaci�n y el n�mero de elementos de los que se compone la malla.

El problema concreto a estudiar es un tubo de longitud $L$ con un extremo cerrado y otro con una perturbaci�n de presi�n de la forma $p_p(t) = \bar{p} e^{i \omega t}$. Se pretende obtener la distribuci�n de presiones en el dominio. El tubo tiene adem�s un elemento de �rea $S$, inercia $J$, rigidez $k$ y coeficiente de amortiguamiento $\gamma$ en el punto medio del tubo (ver figura~\ref{fig:esquema_intro}). Los valores se pueden encontrar en la tabla~\ref{tab:param}.

\begin{figure}[htpb!]
    \centering
    \includegraphics[width=0.8\textwidth]{fig/figura_intro}
    \caption[Esquema del problema a estudiar.]{Esquema del problema a estudiar. El problema consta de dos regiones diferenciadas que se denotar�n a lo largo del desarrollo como $I$ y $II$, tal y como aparece en la figura.}
    \label{fig:esquema_intro}
\end{figure}

\begin{table}[htpb!]
    \centering
    \caption{Par�metros del problema a estudiar.}
    \label{tab:param}
    \begin{tabular}{lrl}
        \toprule
        \textbf{Par�metro} & \textbf{Valor} & \textbf{Unidades} \\
        \midrule
        Longitud del tubo, $L$         & \num{1}       & \si{\m} \\
        Densidad, $\rho$      & \num{1.225}   & \si{\kg\per\cubic\m} \\
        Amplitud de la perturbaci�n de presi�n, $\bar p_p$    & \num{100}     & \si{\Pa} \\
        �rea transversal del tubo, $S$         & \num{0.05}    & \si{\m\squared} \\
        Inercia del elemento central, $J$         & \num{0.05}    & \si{\kg} \\
        Rigidez del elemento central, $k$         & \num{5}       & \si{\kilo\N\per\m} \\
        Coeficiente de amortiguamiento del elemento central, $\gamma$    & \num{0.05}    & \si{} \\
        Velocidad del sonido, $c$ & \num{340} & \si{\m\per\s} \\
        Presi�n ambiente, $p_{\text{atm}}$ & \num{101325} & \si{\Pa} \\
        \bottomrule
    \end{tabular}
\end{table}

\section{Planteamiento anal�tico}

Siendo este un problema de relativa sencillez, es posible encontrar una soluci�n anal�tica con la que poder comparar los resultados de la resoluci�n num�rica que se plantear� en la secci�n~\ref{sec:efa}. A lo largo de este desarrollo se mantendr� en lo posible la notaci�n mostrada en clase.

La ecuaci�n a resolver en el dominio introducido anteriormente es:
\begin{equation*}
    \pdv[2]{p}{x} = \frac{1}{c^2} \pdv[2]{p}{t}
\end{equation*}

Si se aplica el \emph{m�todo de separaci�n de variables}:
\begin{equation*}
    p(x,t) = X(x) T(t)
\end{equation*}

Sustituyendo,
\begin{equation*}
    X''\cdot T = \frac{1}{c^2} X\cdot T'' \Longrightarrow \frac{X''}{X} = \frac{1}{c^2} \frac{T''}{T} = \text{cte} = -\kappa^2
\end{equation*}
y resolviendo cada una de las partes por separado
\begin{align*}
    X'' + \kappa^2 X = 0 & \longrightarrow X(x) = A e^{i\kappa x} + B e^{-i\kappa x}\\
    T'' + \omega^2 T = 0 & \longrightarrow T(t) = C e^{i\omega t} + D e^{-i\omega t}
\end{align*}

Volviendo a la expresi�n principal
\begin{equation*}
    p(x,t) = (A e^{i\kappa x} + B e^{-i\kappa x}) \cdot (C e^{i\omega t} + D e^{-i\omega t})
\end{equation*}

Como la perturbaci�n es de la forma $p_p(t) \sim e^{i\omega t}$, se puede suponer que la parte temporal $T(t)$ ser� de la misma forma, por lo que $D=0$. Adem�s, se pueden redefinir las constantes $A$ y $B$ para incluir $C$. As�,

\begin{equation*}
    p(x,t) = (A e^{i\kappa x} + B e^{-i\kappa x}) \cdot e^{i\omega t}
\end{equation*}

Antes de particularizar la soluci�n a las condiciones de contorno de este problema, conviene recordar c�mo se introduce la velocidad a trav�s de la ecuaci�n de Euler, puesto que ser� de utilidad en la aplicaci�n de las condiciones de contorno:
\begin{align*}
    \pdv{v}{t} &= -\frac{1}{\rho} \pdv{p}{x} \qq{Ecuaci�n de Euler} \\
    \int \pdv{v}{t} \dd t &= - \frac{1}{\rho} \int \left\{ i\kappa\left(A e^{i\kappa x} - B e^{-i\kappa x} \right) e^{i\omega t} \right\} \dd t \\
    v(x,t) &= - \frac{\kappa}{\omega \rho}  \left(A e^{i\kappa x} - B e^{-i\kappa x} \right) e^{i\omega t}
\end{align*}

\subsection{Regi�n izquierda, $p_{I}(x_{I},t)$}
\begin{equation*}
\begin{cases}
    \label{eq:pi:preCC}
    p_{I}(x_{I},t) = (A_{I} e^{i\kappa x_{I}} + B_{I} e^{-i\kappa x_{I}}) \cdot e^{i\omega t} \text{ con $x_I \in [0, L/2]$}\\
    \text{Condiciones de contorno }
    \begin{cases}
        p(x_{I} = 0  , t) = p_p(t) = \bar p_p e^{i\omega t} & \qq{CC.I.1}\\
        v(x_{I} = L/2, t) = \dot\xi(t) & \qq{CC.I.2}
    \end{cases}
\end{cases}
\end{equation*}

Al aplicar la condici�n CC.I.1 sobre $p_{I}$ se llega a
\begin{equation*}
    p_I (x_I = 0, t) = p_p(t) = \bar p_p e^{i\omega t} = (A_I + B_I) e^{i\omega t} \Longrightarrow \bar p_p = A_I + B_I
\end{equation*}
mientras que de CC.I.2 se extrae que
\begin{equation*}
    - i \rho c \omega \bar \xi = A_I e^{i\kappa L/2} - B_I e^{-i\kappa L/2}
\end{equation*}

Combinando ambas expresiones adecuadamente se puede ver que
\begin{align*}
    A_I &= \frac{\bar p_p e^{-i\kappa L/2} - i\rho c\omega \bar \xi}{2\cos(\kappa L/2)}\\
    B_I &= \bar p_p - \frac{\bar p_p e^{-i\kappa L/2} - i\rho c\omega \bar \xi}{2\cos(\kappa L/2)}
\end{align*}

Finalmente,
\begin{equation}\label{eq:p1}
    % p_{I}(x_{I},t) = \left(\frac{\bar p_p e^{-i\kappa L/2} - i\rho c\omega \bar \xi}{2\cos(\kappa L/2)} e^{i\kappa x_{I}} + \left(\bar p_p - \frac{\bar p_p e^{-i\kappa L/2} - i\rho c\omega \bar \xi}{2\cos(\kappa L/2)}\right) e^{-i\kappa x_{I}}\right) \cdot e^{i\omega t}
    p_{I}(x_{I},t) = \left( \left\{\frac{\bar p_p e^{-i\kappa L/2} - i\rho c\omega \bar \xi}{\cos(\kappa L/2)}\right\}  i \sin(\kappa x_I) + \bar p_p e^{-i \kappa x_I}\right) e^{i\omega t}
\end{equation}

\subsection{Regi�n derecha, $p_{II}(x_{II},t)$}
\begin{equation*}
    \begin{cases}
        p_{II}(x_{II},t) = (A_{II} e^{i\kappa x_{II}} + B_{II} e^{-i\kappa x_{II}}) \cdot e^{i\omega t} \text{ con $x_{II} \in [0, L/2]$, a partir de $x_I = L/2$} \\
        \text{Condiciones de contorno }
        \begin{cases}
            v(x_{II} = 0  , t) = \dot\xi(t) & \qq{CC.II.1}\\
            v(x_{II} = L/2, t) = 0 & \qq{CC.II.2}
        \end{cases} 
    \end{cases}
\end{equation*}
    
La condici�n CC.II.1 indica que
\begin{equation*}
    - \omega^2 \bar \xi = - \frac{i\kappa}{\rho}\left(A_{II} - B_{II}\right)
\end{equation*}
mientras que de CC.II.2 se obtiene que
\begin{equation*}
    v = 0 = \frac{-i\kappa}{i\omega\rho}\left( A_{II} e^{i\kappa L/2} - B_{II}e^{-i\kappa L/2} \right) e^{i \omega t} \Longrightarrow A_{II} = B_{II} e^{-i\kappa L}
\end{equation*}

Con ambas relaciones es posible llegar a
\begin{align*}
    A_{II} &= \frac{i\rho c \omega \bar \xi}{1 - e^{-i\kappa L}} e^{-i\kappa L} \\
    B_{II} &= \frac{i\rho c \omega \bar \xi}{1 - e^{-i\kappa L}}
\end{align*}

Por lo que
\begin{equation}\label{eq:p2}
    p_{II}(x_{II},t) = -\frac{i\rho c \omega \bar \xi}{1 - e^{-i\kappa L}} \left( e^{-i \kappa L} e^{i \kappa x_{II}} + e^{-i \kappa x_{II}}  \right) e^{i\omega t}
\end{equation}

\subsection{Acoplamiento, $\xi(t)$}

Los datos del problema relacionados con el elemento central vienen dados en funci�n del coeficiente de amortiguamiento $\gamma$, que se define como $\gamma = \frac{F}{2\sqrt{Jk}}$

\begin{equation*}
    \left( -\omega^2 J + i\omega F + k \right) \bar \xi e^{i\omega t} = S \left[ p_{I}(x_I = L/2, t)  - p_{II}(x_{II} = 0, t) \right]
\end{equation*}

Sustituyendo y manipulando adecuadamente las expresiones se llega a:
\begin{equation}\label{eq:xi}
    \bar \xi = \frac{\bar p_p e^{-i\kappa L/2}\left( 1 + i \tan(\kappa L /2) \right)}{ i\rho \omega c \left( \varepsilon +  i \tan(\kappa L /2)\right) + \eta/S}
\end{equation}
donde se han definido $\varepsilon$ y $\eta$ como
\begin{equation*}
        \varepsilon = \frac{1 + e^{-i\kappa L}}{1 - e^{-i\kappa L}} \qquad \eta = -\omega^2 J + i\omega F + k
    \end{equation*}

\vspace{1cm}

Encontrada una relaci�n funcional de $\bar \xi$ en \eqref{eq:xi} con los par�metros dados, el problema queda resuelto. La soluci�n vendr�a dada por las expresiones \eqref{eq:p1} y \eqref{eq:p2}.
    
\section{Planteamiento num�rico mediante EFA}\label{sec:efa}

\subsection{Desarrollo matem�tico}

El objetivo principal de este trabajo, como ya se ha mencionado, es la resoluci�n num�rica de este problema haciendo uso del m�todo de los elementos finitos, es un variedad para la ac�stica.

El problema consta de dos regiones separadas por una placa, y esto supondr� tener que aplicar condiciones de vibroac�stica. El planteamiento es semejante para cada regi�n, por lo que en este documento se desarrolla el �lgebra de una de ellas, en gen�rico. Luego, se expone la manera en la que se acoplan las dos dentro de un mismo problema y en la que se imponen las condiciones de contorno.

En este marco, las matrices de inercia, amortiguamiento y rigidez para elementos ac�sticos vienen desarrolladas en la literatura, por lo que simplemente se mostrar�n a continuaci�n:

\begin{equation*}
    [M_e] = \frac{\ell_e}{6 c^2}
    \begin{bmatrix}
        2 & 1 \\
        1 & 2 \\
    \end{bmatrix}
    \qquad
    [C_e] = 
    \begin{bmatrix}
        0 & 0 \\
        0 & 0 \\
    \end{bmatrix}
    \qquad
    [K_e] = \frac{1}{\ell_e}
    \begin{bmatrix}
        1 & -1 \\
        -1 & 1 \\
    \end{bmatrix}
\end{equation*}
donde $\ell_e$ es la longitud del elemento, que es constante en cada regi�n si se hace un mallado equiespaciado para cada regi�n. A lo largo de este desarrollo se mantendr� esa hip�tesis. Resaltar que es importante hacer un mallado para cada regi�n $r$\footnote{$r$ indicar� la regi�n correspondiente, y se utilizar� como super�ndice. Su valor ser� $I$ � $II$.}, ya que el nodo coincidente con la placa ha de aparecer dos veces para poder distinguir la diferencia de presiones que parece en cada lado.

Las matrices $[M_e]$, $[C_e]$ y $[K_e]$ se acoplan seg�n la coincidencia de nodos entre $N_e$ elementos, quedando unas matrices $[M_a^r]$, $[C_a^r]$\footnote{En este caso, queda la matriz nula.} y $[K_a^r]$ de dimensiones $(N_e^r+1) \times (N_e^r+1)$:

\begin{equation*}
    [M_a^r] = \frac{\ell_e^r}{6 c^2}
    \begin{bmatrix}
        2 & 1 & 0 &        &   &   &   \\
        1 & 4 & 1 &        &   & 0 &   \\
        0 & 1 &   &        &   &   &   \\
          &   &   & \ddots &   &   &   \\
          &   &   &        &   & 1 & 0 \\
          & 0 &   &        & 1 & 4 & 1 \\
         &   &   &        & 0 & 1 & 2 \\
    \end{bmatrix}_{(N_e^r+1) \times (N_e^r+1)}
\end{equation*}
    % \quad
\begin{equation*}
    [K_a^r] = \frac{1}{\ell_e^r}
    \begin{bmatrix}
         1  & -1 &  0 &        &    &    &    \\
        -1  &  2 & -1 &        &    &  0 &    \\
         0  & -1 &    &        &    &    &    \\
            &    &    & \ddots &    &    &    \\
            &    &    &        &    & -1 &  0 \\
            &  0 &    &        & -1 &  2 & -1 \\
            &    &    &        &  0 & -1 &  1 \\
    \end{bmatrix}_{(N_e^r+1) \times (N_e^r+1)}
\end{equation*}

Cuando se introduce la segunda regi�n ($II$), se anexionan las matrices $[M_a^{II}]$, $[C_a^{II}]$ y $[K_a^{II}]$ a las de la regi�n $I$. Para una matriz gen�rica $[A_a]$:
\begin{equation*}
    [A_a] = \begin{bmatrix}
        \left[A_a^{I} \right] & \left[0       \right] \\
        \left[0       \right] & \left[A_a^{II}\right] \\
    \end{bmatrix}_{(N_e+2) \times (N_e+2)}
\end{equation*}

La ecuaci�n de resoluci�n dentro del problema ac�stica se puede plantear ya como

\begin{equation*}
    \left[ -\omega^2[M_a] + i\omega[C_a] + [K_a] \right] \{p\} = \{q\}
\end{equation*}
donde $\{p\}$ es el vector que contiene la presi�n ac�stica en cada uno de los nodos, mientras que $\{q\}$ la fuerza o la fuente que act�a en cada uno.

Para considerar el elemento central que divide en dos partes el tubo, hay que introducir el problema estructural de tal forma que quede acoplado a la parte ac�stica. Para ello, si se asume que siempre quedar� en uno de los nodos de la malla, se definen las matrices equivalentes para el problema estructural:

\begin{equation*}
    \left[M_s\right] = \left[J\right]_{1 \times 1} \qquad \left[C_s\right] = \left[2\gamma\sqrt{Jk}\right]_{1 \times 1} \qquad \left[K_s\right] = \left[k\right]_{1 \times 1}
\end{equation*}

A continuaci�n, se a�aden estas matrices a las equivalentes del problema ac�stico, para que el acoplamiento quede cerrado.

\begin{align*}
        [M] &= \begin{bmatrix}
            [M_s] & [0] \\
            [M_c] & [M_a] \\
        \end{bmatrix}_{(N_e+3) \times (N_e+3)}
        % \qquad
        \\
        [C] &= \begin{bmatrix}
            [C_s] & [0] \\
            [0]   & [C_a] \\
        \end{bmatrix}_{(N_e+3) \times (N_e+3)}
        % \qquad
        \\
        [K] &= \begin{bmatrix}
            [K_s] & [K_c] \\
            [0]   & [K_a] \\
        \end{bmatrix}_{(N_e+3) \times (N_e+3)}
        % \qquad
        \\
    \{P\} &= \begin{Bmatrix}
        \{w\}\\
        \{p\} \\
    \end{Bmatrix}_{(N_e+3) \times 1}
    % \qquad
    \\
    \{Q\} &= \begin{Bmatrix}
        \{0\}\\
        \{q\} \\
    \end{Bmatrix}_{(N_e+3) \times 1}
\end{align*}

$[K_c]$ y $[M_c]$ son matrices columna y fila respectivamente que son las encargadas de encajar ambas naturalezas del problema, y se definen, si las unidades empleadas son las adecuadas, como:
\begin{equation*}
    [K_c] = \left[\underbrace{0 \;\; \cdots \;\; 0 \;\; -S}_{N_e^{I}+1} \;\; \underbrace{S \;\; 0 \;\; \cdots \;\; 0}_{N_e^{II}+1}\right]
    \qquad [M_c] = - \frac{\rho}{S} [K_c]^T
\end{equation*}

Con esto, se puede completar el sistema hasta llegar a

\begin{equation*}
    \underbrace{\left[ -\omega^2[M] + i\omega[C] + [K] \right]}_{[A]} \{P\} = \{Q\}
\end{equation*}

El siguiente paso es aplicar las condiciones de contorno al modelo num�rico. De las condiciones vistas en la resoluci�n anal�tica, solamente quedan por aplicar CC.I.1 y CC.II.2, dado que el acoplamiento estructural introduce las dos restantes.

La condici�n de pared cerrada (CC.II.2) no requiere una acci�n directa, puesto que al ser condici�n de velocidad nula, no se hace necesaria la introducci�n de ning�n t�rmino no nulo en el modelo.

La condici�n de presi�n (CC.I.1) requiere algo de trabajo. En este caso, al ser una presi�n es una de las inc�gnitas del vector $\{p\}$ si uno de los nodos coincide con la pared (se supondr� que s�). 
Sin embargo esta presi�n es de valor conocido, por lo que �ste debe plasmarse en el modelo de alguna manera.
Para ello, se agrupan los t�rminos entre los correspondientes a nodos con valores conocidos (sub�ndice $c$) y los de valores desconocidos (sub�ndice $d$).
Es posible entonces reordenar las filas y columnas de las matrices seg�n corresponda para que el problema quede de la forma
\begin{equation*}
    \begin{bmatrix}
        [A_{dd}] & [A_{dc}] \\
        [A_{cd}] & [A_{cc}] \\
    \end{bmatrix}
    \begin{Bmatrix}
        \{P_{d}\}\\
        \{P_{c}\} \\
    \end{Bmatrix}
    =
    \begin{Bmatrix}
        \{F_{d}\}\\
        \{F_{c}\} \\
    \end{Bmatrix}
\end{equation*}

En este problema, �nicamente se conoce la presi�n en uno de los nodos, el coincidente con $x=0$ (condici�n de contorno CC.I.1). Si el primer nodo se coloca en esa posici�n, el conjunto de �ndices correspondiente a cada caso ser�a
\begin{gather*}
    d \in \{1, 3, 4, 5, \cdots, (N_e+3)\}\\
    c \in \{2\}
\end{gather*}
para $[A]$, $\{P\}$ y $\{F\}$.

Finalmente, se resuelve la ecuaci�n para $\{P_d\}$, introducciendo en $\{P_c\}$ la condici�n CC.I.1. As�,

\begin{equation*}
    \{P_{d}\} = [A_{dd}]^{-1} \left(\{F_{d}\} - [A_{dc}] \{P_c\}\right)
\end{equation*}

Poniendo juntos de nuevo $\{P_d\}$ y $\{P_c\}$ en $\{P\}$, se extraen los valores de la presi�n en los nodos $\{p\}$.

\subsection{Implementaci�n}

El informe va acompa�ado de dos \textsl{scripts} que resuelven el problema de manera equivalente en dos lenguajes de programaci�n distintos, Python 3.6.3 y Julia 0.6.2. 

El utilizado para obtener los resultados que se muestran en la secci�n~\ref{sec:res} es \texttt{efa.py}, el correspondiente al primero, y hace uso de las librer�as abiertas Numpy 1.14.0\footnote{\url{http://www.numpy.org/}} (capacidades algebraicas y num�ricas) y Matplotlib 2.1.1\footnote{\url{https://matplotlib.org/}} (graficado). La implementaci�n es mejorable ya que no hace uso de matrices dispersas o \textsl{sparse}, por lo que su consumo en memoria ser� alto si se da un valor alto al n�mero de elementos a emplear en la resoluci�n.

La segunda implementaci�n (\texttt{efa.jl}) se ha hecho con el mero motivo de probar las capacidades de Julia\footnote{\url{https://julialang.org/}}, un nuevo lenguaje abierto enfocado al c�lculo cient�fico, y los resultados han sido muy satisfactorios. La sintaxis es sencilla, legible y f�cilmente aplicable para alguien familiarizado con Matlab o Python, adem�s de tener unos tiempos de ejecuci�n interesantes. Esta implementaci�n s� hace uso de matrices \textsl{sparse} debido a la sencillez que supone su uso.

Como nota, conviene abrir los c�digos fuente con un editor que soporte UTF-8, debido a que se han empleado caracteres de esta codificaci�n en los nombres de las variables.


\section{Resultados}\label{sec:res}

Los resultados que se presentan a continuaci�n muestran el valor obtenido de la presi�n ac�stica $p(x,t)$ para distintas frecuencias (\numlist{10;100;200;300;1000;10000} \si{\Hz}), tanto anal�ticamente como num�ricamente. 
Se ha tomado la parte real de la soluci�n encontrada en el dominio complejo. 
En la figuras se utiliza $N$ en vez de $N_e$ para indicar el n�mero de elementos considerado.


La figura~\ref{fig:p_analitica_xt} (p�g.~\pageref{fig:p_analitica_xt}) muestra la evoluci�n temporal y espacial de la soluci�n anal�tica para las frecuencias mencionadas. 
Algunos comentarios interesantes pueden hacerse al respecto. Uno de ellos es el desfase que aparece a ambos lados del elemento central, fruto probablemente de la amortiguaci�n debida a $\gamma$. 
Para la mayor�a de las frecuencias mostradas adem�s la regi�n $II$ aparentemente disminuye su amplitud, aunque es cierto que es posible a frecuencias bajas no haya alcanzado el m�ximo en la regi�n mostrada. 
Esto no sucede a \SI{100}{\Hz}, en la se produce un aumento de la amplitud de la regi�n derecha del tubo. 
Otro aspecto relevante a comentar es lo que ocurre a \SI{200}{\Hz}, donde parece que se ha encontrado una frecuencia cercana a una de las naturales del sistema (la amplitud aumenta de forma considerable frente a la de perturbaci�n). 
Esta ser� una frecuencia a tener en cuenta en los siguientes estudios.

\begin{figure}[htpb!]
    \centering
    \begin{minipage}{0.495\textwidth}
        \includegraphics[width=\textwidth]{fig/p_analitica_xt_frec_10.pdf}
    \end{minipage}
    \begin{minipage}{0.495\textwidth}
        \includegraphics[width=\textwidth]{fig/p_analitica_xt_frec_100.pdf}
    \end{minipage}
    \begin{minipage}{0.495\textwidth}
        \includegraphics[width=\textwidth]{fig/p_analitica_xt_frec_200.pdf}
    \end{minipage}
    \begin{minipage}{0.495\textwidth}
        \includegraphics[width=\textwidth]{fig/p_analitica_xt_frec_300.pdf}
    \end{minipage}
    \begin{minipage}{0.495\textwidth}
        \includegraphics[width=\textwidth]{fig/p_analitica_xt_frec_1000.pdf}
    \end{minipage}
    \begin{minipage}{0.495\textwidth}
        \includegraphics[width=\textwidth]{fig/p_analitica_xt_frec_10000.pdf}
    \end{minipage}
    \caption[Evoluci�n espacial y temporal de la soluci�n anal�tica $p(x,t)$ a distintas frecuencias.]{Evoluci�n espacial y temporal de la soluci�n anal�tica $p(x,t)$ a distintas frecuencias. Destacar los cambios tanto de escala temporal como de amplitud de la presi�n (barra de color).}
    \label{fig:p_analitica_xt}
\end{figure}

La	soluci�n num�rica obtenida para las frecuencias de estudio se muestra en la figura~\ref{fig:x-p_numNodosRed_f} (p�g.~\pageref{fig:x-p_numNodosRed_f}), con el error correspondiente en la figura~\ref{fig:x-p_err_Red_f} (p�g.~\pageref{fig:x-p_err_Red_f}). 
Es f�cil ver que los resultados son muy buenos cuando los frecuencias son bajas (\num{10}, \num{100} e incluso \SI{300}{\Hz})  mientras que empeoran considerablemente a medida que aumenta la frecuencia \num{1000} y \SI{10000}{\Hz}) o se encuentra cerca de una frecuencia de resonancia (\SI{200}{\Hz}), donde es necesario un n�mero m�s elevado de elementos para capturar mejor la soluci�n.

\begin{figure}[htpb!]
	\centering
	\begin{minipage}{0.495\textwidth}
		\includegraphics[width=\textwidth]{figRed/x-p_numNodos_f10.pdf}
	\end{minipage}
	\begin{minipage}{0.495\textwidth}
		\includegraphics[width=\textwidth]{figRed/x-p_numNodos_f100.pdf}
	\end{minipage}
	\begin{minipage}{0.495\textwidth}
		\includegraphics[width=\textwidth]{figRed/x-p_numNodos_f200.pdf}
	\end{minipage}
	\begin{minipage}{0.495\textwidth}
		\includegraphics[width=\textwidth]{figRed/x-p_numNodos_f300.pdf}
	\end{minipage}
	\begin{minipage}{0.495\textwidth}
		\includegraphics[width=\textwidth]{figRed/x-p_numNodos_f1000.pdf}
	\end{minipage}
	\begin{minipage}{0.495\textwidth}
		\includegraphics[width=\textwidth]{figRed/x-p_numNodos_f10000.pdf}
	\end{minipage}
	\caption[Soluci�n num�rica en funci�n de la posici�n y del n�mero de elementos considerado, a distintas frecuencias y para el instante inicial.]{Soluci�n num�rica en funci�n de la posici�n y del n�mero de elementos considerado, a distintas frecuencias y para el instante inicial.}
	\label{fig:x-p_numNodosRed_f}
\end{figure}

\begin{figure}[htpb!]
	\centering
	\begin{minipage}{0.495\textwidth}
		\includegraphics[width=\textwidth]{figRed/x-p_err_f10.pdf}
	\end{minipage}
	\begin{minipage}{0.495\textwidth}
		\includegraphics[width=\textwidth]{figRed/x-p_err_f100.pdf}
	\end{minipage}
	\begin{minipage}{0.495\textwidth}
		\includegraphics[width=\textwidth]{figRed/x-p_err_f200.pdf}
	\end{minipage}
	\begin{minipage}{0.495\textwidth}
		\includegraphics[width=\textwidth]{figRed/x-p_err_f300.pdf}
	\end{minipage}
	\begin{minipage}{0.495\textwidth}
		\includegraphics[width=\textwidth]{figRed/x-p_err_f1000.pdf}
	\end{minipage}
	\begin{minipage}{0.495\textwidth}
		\includegraphics[width=\textwidth]{figRed/x-p_err_f10000.pdf}
	\end{minipage}
	\caption{Diferencia de la soluci�n num�rica respecto a la soluci�n anal�tica en funci�n de la posici�n y del n�mero de elementos considerado, para distintas frecuencias y para el instante inicial.}
	\label{fig:x-p_err_Red_f}
\end{figure}

La influencia del n�mero de elementos y la frecuencia en el error obtenido se puede ver con m�s claridad en la figura~\ref{fig:n-f_errMax} (p�g.~\pageref{fig:n-f_errMax}), donde se ha realizado un barrido m�s amplio de frecuencias y m�s fino de n�mero de elementos. 
Es aqu� donde se observa a la perfecci�n como a medida que se sube la frecuencia se hace necesario aumentar considerablemente el n�mero de elementos a utilizar.
Esto se produce seguramente debido a que el mallado no es lo suficientemente fino en relaci�n a la longitud de onda que frecuencias altas como las empleadas tienen, por lo que es posible que baste con disminuir la longitud de los elementos.
Es interesante tambi�n ver como a pesar de aumentar el n�mero de elementos, no se llega a alcanzar una buena aproximaci�n para el entorno de la frecuencia de \SI{200}{\Hz}, donde se produce una resonancia.
Otras l�neas verticales amarillas que aparecen son posiblemente otras frecuencias propias.


\begin{figure}[htb!]
	\centering
%	\includegraphics[width=0.7\textwidth]{fig/n-f_errMaxPert.pdf}
		\includegraphics[width=\textwidth]{figNF/nf.eps}
	\caption[Evoluci�n de la diferencia m�xima entre la soluci�n num�rica y la soluci�n anal�tica en funci�n de la frecuencia y el n�mero de elementos considerado.]{Evoluci�n de la diferencia m�xima entre la soluci�n num�rica y la soluci�n anal�tica en funci�n de la frecuencia y el n�mero de elementos considerado. Estos resultados han sido obtenidos con \texttt{efa.jl}.}
	\label{fig:n-f_errMax}
\end{figure}

Por �ltimo, se analiza la convergencia de la soluci�n num�rica con la soluci�n anal�tica para las frecuencias de estudio seg�n el n�mero de elementos en la figura~\ref{fig:n-amp_p_f} (p�g.~\pageref{fig:n-amp_p_f}). 
Los comentarios que se pueden hacer son semejantes a los anteriores. 
A frecuencias bajas todo funciona bien mientras que a frecuencias altas empiezan a aparecer oscilaciones en la convergencia, hasta el punto que en \SI{10000}{\Hz} el n�mero m�ximo de elementos no puede considerarse convergida la soluci�n.

\begin{figure}[htpb!]
	\centering
    \begin{minipage}{0.495\textwidth}
        \includegraphics[width=\textwidth]{figRedF/n-amp_p_f10.pdf}
    \end{minipage}
    \begin{minipage}{0.495\textwidth}
        \includegraphics[width=\textwidth]{figRedF/n-amp_p_f100.pdf}
    \end{minipage}
    \begin{minipage}{0.495\textwidth}
        \includegraphics[width=\textwidth]{figRedF/n-amp_p_f200.pdf}
    \end{minipage}
    \begin{minipage}{0.495\textwidth}
        \includegraphics[width=\textwidth]{figRedF/n-amp_p_f300.pdf}
    \end{minipage}
    \begin{minipage}{0.495\textwidth}
        \includegraphics[width=\textwidth]{figRedF/n-amp_p_f1000.pdf}
    \end{minipage}
    \begin{minipage}{0.495\textwidth}
        \includegraphics[width=\textwidth]{figRedF/n-amp_p_f10000.pdf}
    \end{minipage}
	\caption{Evoluci�n de la amplitud de la onda de presi�n de cada regi�n resultante de la implementaci�n num�rica, comparada con la obtenida anal�ticamente, seg�n el n�mero de nodos, y para el instante inicial.}
	\label{fig:n-amp_p_f}
\end{figure}

% \begin{figure}[htpb!]
%     \centering
%     \includegraphics[width=0.9\textwidth]{fig/n-f_errMaxRel.pdf}
%     \caption[Evoluci�n de la diferencia m�xima entre la soluci�n num�rica y la soluci�n anal�tica en funci�n de la frecuencia y el n�mero de elementos considerado, adimensionalizado con el m�ximo de la soluci�n anal�tica.]{Evoluci�n de la diferencia m�xima entre la soluci�n num�rica y la soluci�n anal�tica en funci�n de la frecuencia y el n�mero de elementos considerado, adimensionalizado con el m�ximo de la soluci�n anal�tica.}
%     \label{fig:n-f_errMaxRel}
% \end{figure}

\clearpage

\section{Conclusiones}

En el desarrollo de este trabajo se ha implementado una resoluci�n num�rica de un problema vibroac�stico mediante el m�todo de los elementos finitos. 

Algunos comentarios interesantes que se pueden rescatar de los resultados presentados se resumen en que a frecuencias altas parece necesario aumentar sensiblemente el n�mero de elementos a utilizar, por lo que el coste computacional crece significativamente con la frecuencia.
Adem�s, parece que el m�todo no es capaz de capturar bien la soluci�n alrededor de las frecuencias naturales del sistema.

En cuanto a la implementaci�n, se pueden mejorar algunos aspectos como el uso de matrices \textsl{sparse} y rutinas especializadas en ellas, puesto que deber�an disminuir tanto el tiempo de c�lculo como principalmente el consumo de memoria. 
Esto es especialmente relevante en este problema ya que se ha visto en el planteamiento num�rico que las matrices son principalmente tridiagonales.

Un �ltimo comentario viene en relaci�n a la soluci�n obtenida. 
En una visita al profesor se compar� el resultado conseguido con la soluci�n anal�tica y se vio que en la regi�n derecha se llegaba a la soluci�n opuesta. 
Tras repasar varias veces el desarrollo, no se ha encontrado ese signo negativo que parece que falta en las constantes $A_{II}$ y $B_{II}$. 
Adem�s, se ha llegado a la misma soluci�n con el m�todo num�rico, que tambi�n se ha repasado e implementado varias veces. 
Puesto que no se ha logrado salvar esa diferencia, se han presentado estos resultados aparentemente err�neos.
Sin embargo las conclusiones que se han extra�do parecen igualmente v�lidas.

\end{document}
