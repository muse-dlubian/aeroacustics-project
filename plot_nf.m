err = load('jul/n-f_err.txt');
f = load('jul/frecuencias.txt');
for i =1:13
    ne(i) = 2^i;
end

ne = load('jul/ne.txt');

figure(1)
[nnn, fff] = meshgrid(ne, f);
contourf(fff, nnn, log10(err));
xlabel("Frecuencia [Hz]")
ylabel("Número de elementos")
title("Error, log_{10} máx|p - p_{ana}| [Pa]")
colorbar()


t_cal = load("jul/t_cal.txt");

figure(3)
tcal_m = mean(tcal, 1)

figure(2)
[nnn, fff] = meshgrid(ne, f);
contourf(fff, nnn, log10(t_cal));
xlabel("Frecuencia [Hz]")
ylabel("Número de elementos")
title("Tiempo de cálculo")
colorbar()