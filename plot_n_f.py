#from efa import plot_as_latex
import matplotlib.pyplot as plt
import numpy as np

def plot_as_latex(font="lmodern", size=18, ext=".png", dpi=150):
    """Gives format to the plots in the same
    "enviroment" to have Latin Modern Roman
    or Palatino.

    Parameters
    ---
    font: selected font, between Latin Modern Roman ("lmodern")
            or Palatino ("palatino")
    size: font size
    ext:    extension for savefig
    dpi:    dpi for savefig

    Returns
    ---
    ext:    extension for savefig
    dpi:    dpi for savefig
    """
    import matplotlib

    # fontName = ['Latin Modern Roman' if font == "lmodern"]
    # fontName = ['Palatino' if font == "palatino"]
    # fontName = 'Latin Modern Roman'
    fontName = 'Palatino'

    # Formateo graficas:
    matplotlib.rcParams['text.latex.unicode'] = True
    plt.rc('font', **{'family': 'serif', 'serif': [fontName]})
    # plt.rc('font', **{'family': 'serif', 'serif': ['Latin Modern Roman']})
    # plt.rc('font', **{'family':'serif', 'serif':['Palatino']})
    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath}' +
           r'\usepackage[spanish]{babel}\usepackage{siunitx}')
    matplotlib.rcParams.update({'font.size': size})
    extFig = ext
    # extFig = ".png"
    # extFig = ".pdf"
    # extFig = ".eps"
    # dpinum = 200
    dpinum = dpi
    return extFig, dpinum

dirFig = "figNF/"

num_elem = [2**i for i in range(1,14)]

frecs = np.loadtxt("frecuencias.txt")

errMaxPert = np.loadtxt("n-f_err.txt")

#%% DISTRIBUCION F-N DEL ERROR MAXIMO EFA-ANA ADIM CON P_PERT
import matplotlib.colors as mcolors
plot_as_latex(size=14)
plt.figure()
nnn, fff = np.meshgrid(num_elem, frecs)
plt.contourf(np.array(fff), nnn, np.log10(errMaxPert),
             levels=np.linspace(np.log10(np.min(np.real(errMaxPert))), np.log10(np.max(np.real(errMaxPert))), 151).tolist())
plt.colorbar()
# plt.colorbar().set_label(r"$\log_{10} \frac{\left|p - p_{\text{ana}}\right|}{\bar p} $")
plt.xscale("log")
plt.yscale("log")
plt.xlabel(r"Frecuencia de perturbación, $f$ [\si{\Hz}]")
plt.ylabel(r"Número de elementos, $N$")
plt.title(r"Error calculado como $\log_{10} \frac{\left|p - p_{\text{ana}}\right|}{\bar p} $")
# cb.set_label(r"$\log_{10} \frac{\left|p - p_{\text{ana}}\right|}{\bar p} $")
plt.savefig(dirFig+"n-f_errMaxPert.pdf", bbox_inches="tight")
