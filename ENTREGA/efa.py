# TRABAJO 14
#  Daniel Lubian Arenillas

import numpy as np
import matplotlib.pyplot as plt
π = np.pi

dirFig = "fig/"
dirFig = "figRed/"
dirFig = "figRedF/"

def plot_as_latex(font="lmodern", size=18, ext=".png", dpi=150):
    """Gives format to the plots in the same
    "enviroment" to have Latin Modern Roman
    or Palatino.

    Parameters
    ---
    font: selected font, between Latin Modern Roman ("lmodern")
            or Palatino ("palatino")
    size: font size
    ext:    extension for savefig
    dpi:    dpi for savefig

    Returns
    ---
    ext:    extension for savefig
    dpi:    dpi for savefig
    """
    import matplotlib

    # fontName = ['Latin Modern Roman' if font == "lmodern"]
    # fontName = ['Palatino' if font == "palatino"]
    # fontName = 'Latin Modern Roman'
    fontName = 'Palatino'

    # Formateo graficas:
    matplotlib.rcParams['text.latex.unicode'] = True
    plt.rc('font', **{'family': 'serif', 'serif': [fontName]})
    # plt.rc('font', **{'family': 'serif', 'serif': ['Latin Modern Roman']})
    # plt.rc('font', **{'family':'serif', 'serif':['Palatino']})
    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath}' +
           r'\usepackage[spanish]{babel}\usepackage{siunitx}')
    matplotlib.rcParams.update({'font.size': size})
    extFig = ext
    # extFig = ".png"
    # extFig = ".pdf"
    # extFig = ".eps"
    # dpinum = 200
    dpinum = dpi
    return extFig, dpinum

plot_as_latex()

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%%%%%%% numero de elementos
ne = 100
num_elem = [ne]
num_elem = [4, 8, 16, 32, 64, 128, 256, 512, 1024]
# num_elem = [4, 8, 16, 32, 64]
num_elem = [4, 6, 8, 16, 32, 50, 64, 90, 128, 188, 256, 400, 512, 600, 760, 1024]

#%%%%%%% pulsacion
f = 100
ω = 2 * π * f
frecs = [1, 5, 10, 20, 30, 50, 100, 150, 170, 180, 190, 200,
         210, 220, 250, 300, 400, 500, 600, 750, 1000, 2500,
         5000, 7500, 10000, 15000, 20000, 50000, 100000]
frecs = [10, 100, 200, 300, 1000, 10000]
# frecs = [f, 600]
ωs = 2 * π * np.array(frecs)
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


#%
# Parametros del problema dados

L = 1  # m
p_pert = 100  # Pa
ρ = 1.225  # kg/m^3
S = 0.05  # m^2
J = 0.05  # kg
k = 5e3  # N/m
γ = 0.05  # NO es el coeficiente adiabatico

#%
# Parametros del problema extra
p_atm = 101325.  # Pa. Asumo presion estandar a nivel del mar
c = np.sqrt(p_atm * 1.4 / ρ)
κ = ω / c
F = 2 * γ * np.sqrt(J * k)


def p_analitica(x, t, ω_):
    κ = ω_ / c
    x_aux = x

    #% termino temporal
    et = np.exp(1j * ω_ * t)

    #% termino de acoplamiento
    ε = (1 + np.exp(- 1j * κ * L)) / (1 - np.exp(- 1j * κ * L))
    η = - ω_**2 * J + 1j * ω_ * F + k
    ξ = p_pert * np.exp(- 1j * κ * L / 2)
    ξ *= 1 + 1j * np.tan(κ * L / 2)
    ξ /= 1j * ρ * ω_ * c * (ε + 1j * np.tan(κ * L / 2)) + η / S

    #% calculo constantes
    if x < L / 2:
        cte_A = p_pert * np.exp(-1j * κ * L / 2) - 1j * ρ * ω_ * ξ * c
        cte_A /= 2 * np.cos(κ * L / 2)
        cte_B = p_pert - cte_A
    elif x >= L / 2:
        x_aux -= L/2.
        cte_B =  1j * ρ * ω_ * ξ * c / (1 - np.exp(- 1j * κ * L))
        # cte_B *= -1
        cte_A = cte_B * np.exp(- 1j * κ * L)

    #% solucion completa
    return (cte_A * np.exp(1j * κ * x_aux) + cte_B * np.exp(- 1j * κ * x_aux)) * et

errMaxPert = np.empty((ωs.shape[0], len(num_elem)))
errMaxRel = np.empty((ωs.shape[0], len(num_elem)))
amp = np.empty((4, len(num_elem)))

i_ω = 0
for ω in ωs:
    f = ω/2/π
    print("f %.0f" %(f))

    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()

    i_n = 0
    for ne in num_elem:
        print("  ne ", ne)

        #%
        # Mallado
        ne1 = ne//2
        ne2 = ne//2
        nn1 = ne1 + 1
        nn2 = ne2 + 1
        nn = nn1 + nn2

        x1 = np.linspace(0, L/2 - 1e-8, nn1)
        x2 = np.linspace(L/2, L, nn2)
        x = np.append(x1, x2)

        #%
        # Propiedades del elemento

        L_e1 = x1[1] - x1[0]
        K_e1 = (1 / L_e1) * np.array([[1, -1], [-1, 1]])
        M_e1 = (L_e1 / c**2) * np.array([[1 / 3, 1 / 6], [1 / 6, 1 / 3]])
        L_e2 = x2[1] - x2[0]
        K_e2 = (1 / L_e2) * np.array([[1, -1], [-1, 1]])
        M_e2 = (L_e2 / c**2) * np.array([[1 / 3, 1 / 6], [1 / 6, 1 / 3]])

        #%
        # Parte acustica

        Ma1 = np.zeros((nn1, nn1))
        Ca1 = np.zeros((nn1, nn1))
        Ka1 = np.zeros((nn1, nn1))

        Ma2 = np.zeros((nn2, nn2))
        Ca2 = np.zeros((nn2, nn2))
        Ka2 = np.zeros((nn2, nn2))

        for e in range(0, ne1):
            Ma1[e:e + 2, e:e + 2] += M_e1[:, :]
            Ka1[e:e + 2, e:e + 2] += K_e1[:, :]

        for e in range(0, ne2):
            Ma2[e:e + 2, e:e + 2] += M_e2[:, :]
            Ka2[e:e + 2, e:e + 2] += K_e2[:, :]
        
        Ma = np.append(np.append(Ma1, np.zeros((nn1, nn1)), axis=1), np.append(np.zeros((nn2,nn2)), Ma2, axis=1), axis=0)
        Ca = np.append(np.append(Ca1, np.zeros((nn1, nn1)), axis=1), np.append(np.zeros((nn2,nn2)), Ca2, axis=1), axis=0)
        Ka = np.append(np.append(Ka1, np.zeros((nn1, nn1)), axis=1), np.append(np.zeros((nn2,nn2)), Ka2, axis=1), axis=0)

        #%
        # Parte estructural

        Ms = np.array([[J]])
        Cs = np.array([[2*γ*np.sqrt(k * J)]])
        Ks = np.array([[k]])

        #%
        # Acoplamiento

        Kc = np.zeros((1, nn))
        Kc[0, nn//2 -1] = -S
        Kc[0, nn//2 ] = +S
        Mc = - ρ * Kc.T / S

        K = np.append(Ks, Kc, axis=1)
        K = np.append(K, np.append(np.zeros((nn, 1)), Ka, axis=1), axis=0)

        C = np.append(Cs, np.zeros((1, nn)), axis=1)
        C = np.append(C, np.append(np.zeros((nn, 1)), Ca, axis=1), axis=0)

        M = np.append(Ms, np.zeros((1, nn)), axis=1)
        M = np.append(M, np.append(Mc, Ma, axis=1), axis=0)

        A = K + 1j * ω * C - ω**2 * M

        #%
        # Cargas externas

        Fa = np.zeros((nn, 1), dtype=complex)
        # perturbacion de presion lado izdo
        # Fa[0] += 1j * (ω / c) * p_pert
        # pared cerrada en lado dcho --> velocidad nula
        Fq = np.append(np.zeros((1, 1)), Fa, axis=0)

        #%
        # separar en conocidos (c) y desconocidos (d)
        cc = [i for i in range(1,2)]
        dd = [0]
        dd.extend([i for i in range(2, nn+1)])

        Fq_dd = Fq[dd]
        
        A_cc = np.array([A[i1,i2] for i1 in cc for i2 in cc]).reshape((len(cc), len(cc)))
        A_dd = np.array([A[i1,i2] for i1 in dd for i2 in dd]).reshape((len(dd), len(dd)))
        A_dc = np.array([A[i1,i2] for i1 in dd for i2 in cc]).reshape((len(dd), len(cc)))
        A_cd = np.array([A[i1,i2] for i1 in cc for i2 in dd]).reshape((len(cc), len(dd)))
        np.savetxt("A_dd.txt", np.real(A_dd), fmt="%10.2f", delimiter="  ")
        np.savetxt("A.txt", np.real(A), fmt="%10.2f", delimiter="  ")

        #%
        # Resolucion
        t = 0.
        P_cc = np.array(p_pert * np.exp(1j * ω * t)).reshape((1,1))
        P_dd = np.linalg.solve(A_dd, Fq_dd - A_dc @ P_cc)
        P = np.append(P_dd[0], P_cc)
        P = np.append(P, P_dd[1:])
        w, p = P[0], P[1:]

        #%
        # Solucion analitica

        p_ana = np.array([p_analitica(xi, t, ω) for xi in x], dtype=complex)

        #%
        # Graficas

        ax1.plot(x, np.real(p), label=r"$N = " + "%i" % (ne) + "$")

        ax2.semilogy(x, np.abs(np.real(p) - np.real(p_ana))/p_pert,
                     label=r"$N = " + "%i" % (ne) + "$")

        errMaxPert[i_ω, i_n] = np.max(np.abs(np.real(p) - np.real(p_ana))/p_pert)
        errMaxRel[i_ω, i_n] = np.max(np.abs(np.real(p) - np.real(p_ana))/np.max(np.abs(np.real(p_ana))))
        amp[0, i_n] = np.max(np.abs(p[0:nn1+1]))
        amp[1, i_n] = np.max(np.abs(p_ana[0:nn1+1]))
        amp[2, i_n] = np.max(np.abs(p[nn1+1:nn]))
        amp[3, i_n] = np.max(np.abs(p_ana[nn1+1:nn]))

        #% contadores
        i_n += 1

    #%% FIGURA: SOLUCION EFA EN FUNCION DE NODOS
    ax1.plot(x, np.real(p_ana), label=r"Analítica", c="k", linestyle=":")
    ax1.grid(True)
    ax1.set_xlabel(r"Posici\'on, $x$ [\si{\m}]")
    ax1.set_ylabel(r"Presi\'on, $p$ [\si{\Pa}] ")
    ax1.axvline(L / 2, c="0.7", linestyle="--")
    ax1.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    ax1.set_title(r"Frecuencia: $f = \SI{" + "%.0f" % (f) + r"}{\Hz}$")
    fig1.savefig(dirFig+"x-p_numNodos_f%.0f.pdf" % (f), bbox_inches="tight")


    #%% FIGURA: ERROR EFA vs ANA EN FUNCION DE NODOS
    ax2.grid(True)
    ax2.set_xlabel(r"Posici\'on, $x$ [\si{\m}]")
    ax2.set_ylabel(r"Error, $|p - p_{\text{ana}}|/\bar p$ [\si{\Pa}] ")
    ax2.axvline(L / 2, c="0.7", linestyle="--")
    ax2.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    ax2.set_title(r"Frecuencia: $f = \SI{" + "%.0f" % (f) + r"}{\Hz}$")
    fig2.savefig(dirFig+"x-p_err_f%.0f.pdf" % (f), bbox_inches="tight")

    #%% FIGURA: AMPLITUD FEM CON NUM_ELEM
    fig3, ax3 = plt.subplots(nrows=2, ncols=1, sharex=True, squeeze=True)
    ax3[0].semilogx(num_elem, amp[0,:], label=r"$\max|p_{I}|$")
    ax3[0].semilogx(num_elem, amp[1,:],":", label=r"$\max|p_{I,\text{ana}}|$")
    # ax3[0].axhline(amp[1,-1], c="k", linestyle=":", label=r"$\max|p_{I, \text{ana}}|$")
    ax3[0].legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    ax3[0].grid(True)
    ax3[1].semilogx(num_elem, amp[2,:], label=r"$\max|p_{II}|$")
    ax3[1].semilogx(num_elem, amp[3,:], ":", label=r"$\max|p_{II,\text{ana}}|$")
    # ax3[1].axhline(amp[3,-1], c="k", linestyle=":", label=r"$\max|p_{II, \text{ana}}|$")
    ax3[1].legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    ax3[1].grid(True)
    ax3[1].set_xlabel(r"Número de elementos, $N$")
    # fig3.suptitle(r"Amplitud máxima de la presión, $|p|$ [\si{\Pa}]")
    fig3.suptitle(r"Frecuencia: $f = \SI{" + "%.0f" % (f) + r"}{\Hz}$")
    fig3.savefig(dirFig+"n-amp_p_f%.0f.pdf" % (f), bbox_inches="tight")

    # plt.show()

    #%% PERTURBACION EN X=0, EVOL EN EL TIEMPO
    # plt.figure()
    # plt.title("$p_p(t)$")
    # plt.grid(True)
    # t = np.linspace(0, 3/f, 1000)
    # plt.plot(t, np.real(p_analitica(x=0, t=t, ω_=ω)))


    #%% DISTRIBUCION X-T DE LA SOLUCION ANALITICA
    plt.figure()
    tt = np.linspace(0, 3 / f, 1 + 100)
    xx = np.linspace(0, 1, 1 + 100)
    xxx, ttt = np.meshgrid(xx, tt)
    pana = np.zeros_like(xxx, dtype=complex)
    for i in range(0, ttt.shape[0]):
        for ii in range(0, xxx.shape[1]):
            pana[i, ii] = p_analitica(xxx[0, ii], ttt[i, 0], ω)
    plt.contourf(xxx, ttt, np.real(pana))
    plt.contourf(xxx, ttt, np.real(pana),
                 levels=np.linspace(np.min(np.real(pana)), np.max(np.real(pana)), 51).tolist())
    plt.colorbar()
    plt.xlabel(r"Posición, $x$ [\si{\m}]")
    plt.ylabel(r"Tiempo, $t$ [\si{\s}]")
    plt.title(r"Frecuencia: $f = \SI{" + "%.0f" % (f) + r"}{\Hz}$")
    plt.savefig(dirFig+"p_analitica_xt_frec_%.0f.pdf" % (f), bbox_inches="tight")

    plt.close("all")
    i_ω += 1

#%% DISTRIBUCION F-N DEL ERROR MAXIMO EFA-ANA ADIM CON P_PERT
import matplotlib.colors as mcolors
plot_as_latex(size=14)
plt.figure()
nnn, fff = np.meshgrid(num_elem, frecs)
plt.contourf(np.array(fff), nnn, np.log10(errMaxPert),
             levels=np.linspace(np.log10(np.min(np.real(errMaxPert))), np.log10(np.max(np.real(errMaxPert))), 51).tolist())
plt.colorbar()
# plt.colorbar().set_label(r"$\log_{10} \frac{\left|p - p_{\text{ana}}\right|}{\bar p} $")
plt.xscale("log")
plt.yscale("log")
plt.xlabel(r"Frecuencia de perturbación, $f$ [\si{\Hz}]")
plt.ylabel(r"Número de elementos, $N$")
plt.title(r"Error calculado como $\log_{10} \frac{\left|p - p_{\text{ana}}\right|}{\bar p} $")
# cb.set_label(r"$\log_{10} \frac{\left|p - p_{\text{ana}}\right|}{\bar p} $")
plt.savefig(dirFig+"n-f_errMaxPert.pdf", bbox_inches="tight")


#%% DISTRIBUCION F-N DEL ERROR MAXIMO EFA-ANA ADIM CON MAX P_ANA
plt.figure()
nnn, fff = np.meshgrid(num_elem, frecs)
plt.contourf(np.array(fff), nnn, np.log10(errMaxRel),
             levels=np.linspace(np.log10(np.min(np.real(errMaxRel))), np.log10(np.max(np.real(errMaxRel))), 51).tolist())
plt.colorbar()
# plt.colorbar().set_label(r"$\log_{10} \frac{\left|p - p_{\text{ana}}\right|}{\bar p} $")
plt.xscale("log")
plt.yscale("log")
plt.xlabel(r"Frecuencia de perturbación, $f$ [\si{\Hz}]")
plt.ylabel(r"Número de elementos, $N$")
plt.title(r"Error calculado como $\log_{10} \frac{\left|p - p_{\text{ana}}\right|}{\max |p_{\text{ana}}|} $")
# cb.set_label(r"$\log_{10} \frac{\left|p - p_{\text{ana}}\right|}{\bar p} $")
plt.savefig(dirFig+"n-f_errMaxRel.pdf", bbox_inches="tight")


# #%% ANIMACION DE LA SOLUCION ANALITICA
# import matplotlib.animation as animation
# fig, ax = plt.subplots()
# ax.grid(True)
# ax.axis([0, 1, -200, 200])
# ax.axhline(100, c="r", linestyle="--")
# ax.axhline(-100, c="r", linestyle="--")
# t = np.linspace(0, 5, 1+5000)
# Δt = t[1]-t[0]
# print(Δt)
# x = np.linspace(0, L, 500+1)
# p_ana = np.zeros_like(x, dtype=complex)
# for i in range(0, p_ana.shape[0]):
#     p_ana[i] = p_analitica(x[i], t[0], ω)
# line, = ax.plot(x, np.real(p_ana), color="C0", linestyle="-", marker=".", markerfacecolor='b')
# def animate(tt):
#     for i in range(0, p_ana.shape[0]):
#         p_ana[i] = p_analitica(x[i], tt, ω)
#     print(tt)
#     line.set_ydata(np.real(p_ana))  # update the data
#     return line,
# # ax.set_title("Tiempo: %.3f s" %(tt))
# def init():
#     line.set_ydata(np.ma.array(x, mask=True))
#     return line,
# ani = animation.FuncAnimation(fig, animate, t, init_func=init,
#                               interval=1000*Δt, blit=False)
# plt.show()
# ani.save("ani2.mp4")

plt.close("all")
