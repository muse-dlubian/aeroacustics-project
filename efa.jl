#! ~/julia/bin


# using Gadfly
# using Plotly
# using Plots
# pyplot()
# using GR
# using UnicodePlots


## TRABAJO 14
#  Daniel Lubian Arenillas

###################################
# numero de elementos
# ne = 10000
num_elem = [Int(2*floor(2^(i/2))) for i ∈ 1:1:30]
num_elem = [2:2:10; 12:6:100; 120:20:500; 600:100:2000; 3000:1000:6000]
# pulsacion
# ω = 2π * 100
frecuencias = [1:2:11; 15:5:100; 110:10:300; 350:50:1000; 1500:250:50000]
###################################

##
# Parametros del problema

const L = 1. # m
const p_pert = 100. # Pa
const ρ = 1.225 # kg/m^3
const S = 0.05 # m^2
const J = 0.05 # kg
const k = 5e3 # N/m
const γ = 0.05 # NO es el coeficiente adiabatico
const p_atm = 101325. # Pa. Asumo presion estandar a nivel del mar
# const c = sqrt(p_atm * 1.4 / ρ)
const c = √(p_atm * 1.4 / ρ)
# const κ = ω / c
# const F = 2γ * sqrt(J * k)
const F = 2γ * √(J * k)

##
# funcion analitica

function p_analitica(x, t, ω_)
    x_aux = x
    κ = ω_ / c

    # termino temporal
    et = exp(1im * ω_ * t)

    # termino de acoplamiento
    ε = (1 + exp(- 1im * κ * L)) / (1 - exp(- 1im * κ * L))
    η = - ω_^2 * J + 1im * ω_ * F + k
    ξ = p_pert * exp(- 1im * κ * L/2)
    ξ *= 1 + 1im * tan(κ * L/2)
    ξ /= 1im * ρ * ω_ * c * (ε + 1im * tan(κ * L/2)) + η / S

    # calculo constantes
    if x < L/2
        cte_A = p_pert * exp(-1im * κ * L/2) - 1im * ρ * ω_ * ξ * c
        cte_A /= 2 * cos(κ * L / 2)
        cte_B = p_pert  - cte_A
    elseif x >= L/2
        x_aux -= L/2
        cte_B = 1im * ρ * ω_ * ξ * c / (1 - exp(- 1im * κ * L))
        # cte_B *= -1
        cte_A = cte_B * exp(- 1im * κ * L)
    end
    return (cte_A*exp(1im * κ * x_aux) + cte_B*exp(-1im * κ * x_aux)) * et
end

errMax = zeros(length(frecuencias), length(num_elem))
t_cal = zeros(length(frecuencias), length(num_elem))

for i_f ∈ 1:length(frecuencias)
    f = frecuencias[i_f]
    println("f = ", f, " Hz")
    ω = 2π * f

for i_ne ∈ 1:length(num_elem)
    ne = num_elem[i_ne]
    println("   ne = ", ne)

tic()

##
# Mallado

# numero de elementos
ne1 = ne÷2
ne2 = ne÷2

#numero de nodos
nn1 = ne1 + 1
nn2 = ne2 + 1
nn = nn1 + nn2

# equiespaciado
x1 = linspace(  0, L/2, ne1+1)
x2 = linspace(L/2,   L, ne2+1)
x = [x1; x2]

##
# Propiedades del elemento

L_e1 = x1[2] - x1[1]
K_e1 = (1 / L_e1) * [1 -1 ; -1 1]
M_e1 = (L_e1 / c^2) * [1/3 1/6 ; 1/6 1/3]

L_e2 = x2[2] - x2[1]
K_e2 = (1 / L_e2) * [1 -1 ; -1 1]
M_e2 = (L_e2 / c^2) * [1/3 1/6 ; 1/6 1/3]

##
# Parte acustica

Ma1 = spzeros(nn1, nn1)
Ca1 = spzeros(nn1, nn1)
Ka1 = spzeros(nn1, nn1)

Ma2 = spzeros(nn2, nn2)
Ca2 = spzeros(nn2, nn2)
Ka2 = spzeros(nn2, nn2)

for e ∈ 1:ne1
    Ma1[e:e+1, e:e+1] += M_e1
    Ka1[e:e+1, e:e+1] += K_e1
end

for e ∈ 1:ne2
    Ma2[e:e+1, e:e+1] += M_e2
    Ka2[e:e+1, e:e+1] += K_e2
end

Ka = [Ka1 spzeros(nn1,nn1); spzeros(nn2,nn2) Ka2]
Ca = [Ca1 spzeros(nn1,nn1); spzeros(nn2,nn2) Ca2]
Ma = [Ma1 spzeros(nn1,nn1); spzeros(nn2,nn2) Ma2]

##
# Parte estructural

Ms = spzeros(1,1)
Cs = spzeros(1,1)
Ks = spzeros(1,1)
Ms[1,1] += J
Cs[1,1] += F
Ks[1,1] += k

##
# Acoplamiento

Kc = spzeros(1, nn)
Kc[1, nn1]   -= S
Kc[1, nn1+1] += S
Mc = - ρ * Kc' / S

K = [Ks            Kc; spzeros(nn,1) Ka]
C = [Cs spzeros(1,nn); spzeros(nn,1) Ca]
M = [Ms spzeros(1,nn);            Mc Ma]

A = -ω^2 * M + 1im * ω * C + K

##
# Carga externa

# Fa = zeros(Complex{Float64}, nn, 1)
Fa = spzeros(nn, 1)
# Fa[1] +=  1im * (ω / c) * p_pert
Fq = [0; Fa]

##
# separar en conocidos (c) y desconocidos (d)
cc = 2:2
dd = [1]
append!(dd, [i for i ∈ 3:nn+1])

Fq_dd = Fq[dd]

A_cc = A[cc, cc]
A_dd = A[dd, dd]
A_dc = A[dd, cc]
A_cd = A[cc, dd]

# A_dd_inv = inv(A)

##
# Resolucion numerica

t = 0
P_cc = p_pert * exp(1im * ω * t)
P_dd = A_dd \ (Fq_dd - A_dc * P_cc)
P = [P_dd[1]; P_cc; P_dd[2:end]]

w, p = P[1], P[2:end]

##
# Solucion analitica

p_ana = [p_analitica(xi, t, ω) for xi ∈ x]


##
# Guardar el error y el tiempo de calculo
errMax[i_f, i_ne] = maximum(abs.(real.(p - p_ana)))
t_cal[i_f, i_ne] = toc();

end

end

writedlm("jul/n-f_err.txt", errMax)
writedlm("jul/frecuencias.txt", frecuencias)
writedlm("jul/t_cal.txt", t_cal)

##
#
# plt = plot(x=x, y=real(p), Geom.line, Guide.xlabel("Posición"), Guide.ylabel("Presión"),  xintercept=[L/2], Geom.vline(color=["black"], size=[0.3mm]))
# draw(PDF("myplot.pdf", 16cm, 9cm), plt)

# figure()
# plot(x, real(p_ana), linewidth=2, xlabel="Posicion, x", ylabel="Presion, p", title="Solucion analitica")

##
# figure()
# plot(x, real(p), linewidth=2, xlabel="Posicion, x", ylabel="Presion, p", title="Solucion numerica")

##
# figure()
# semilogy(x, abs.(real(p)-real(p_ana)), xlabel="Posicion, x", ylabel="Error", title="Error en la solucion numerica")
# show()

# axvline(L/2, c="k")
# show(True)
# savefig("efa.jl.pdf")
